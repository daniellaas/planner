  @extends('layouts.app', ['activePage' => 'dashboard', 'titlePage' => __('Dashboard')])

  @section('content')
    <div class="content">
      <div class="container-fluid">

      <div class="nav-tabs-wrapper">
                    <h3><span class="nav-tabs-title">Hello {{ Auth::user()->name}}</span></h3>
                  </div>

        
        <div class="row">
        
          <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
              <div class="card-header card-header-warning card-header-icon">
                <div class="card-icon">
                  <i class="material-icons">info_outline</i>
                </div>
                <p class="card-category">Organization Code</p>
                <h3 class="card-title">{{ Auth::user()->org_id}}
                </h3>
              </div>
              <div class="card-footer">
                <div class="stats">
                  <i class="material-icons text-danger">warning</i>
                  Needed for User Registration
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
              <div class="card-header card-header-info card-header-icon">
                <div class="card-icon">
                  <i class="fa fa-user"></i>
                </div>
                <p class="card-category">Users in my Org</p>
                <h3 class="card-title">{{$users}}</h3>
              </div>
              <div class="card-footer">
                <div class="stats">
                  <i class="material-icons">update</i> Just Updated
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <div class="card card-chart">
              <div class="card-header card-header-success">
                <div class="ct-chart" id="dailySalesChart"></div>
              </div>
              <div class="card-body">
                <h4 class="card-title">Topics Discussed</h4>
                <p class="card-category">
                  Throughout all Meeting Planner's meetings</p>
              </div>
              <div class="card-footer">
                <div class="stats">
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-md-4">
            <div class="card card-chart">
              <div class="card-header card-header-danger">
                <div class="ct-chart" id="completedTasksChart"></div>
              </div>
              <div class="card-body">
                <h4 class="card-title">Number Of Tasks</h4>
                <p class="card-category">For this current logged in user</p>
              </div>
              <div class="card-footer">
                <div class="stats">
                  <i class="material-icons">access_time</i> Data is up tp date
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
              <div class="card-header card-header-success card-header-icon">
                <div class="card-icon">
                  <i class="material-icons">store</i>
                </div>
                <p class="card-category">Planned Meetings</p>
                <h3 class="card-title">{{ $myMeetings }}</h3>
              </div>
              <div class="card-footer">
                <div class="stats">
                  <i class="material-icons">date_range</i> Upcoming meetings you either planned or are invited to
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
              <div class="card-header card-header-danger card-header-icon">
                <div class="card-icon">
                  <i class="material-icons">list</i>
                </div>
                <p class="card-category">Open Tasks</p>
                <h3 class="card-title">{{ $openTasks }}</h3>
              </div>
              <div class="card-footer">
                <div class="stats">
                  <i class="material-icons">check</i> Please finish ASAP
                </div>
              </div>
            </div>
          </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="card card-stats">
                          <div class="card-header card-header-info card-header-icon">
                            <div class="card-icon">
                            <i class="material-icons">list</i>
                            </div>
                            <p class="card-category">Delayed tasks</p>
                            <h3 class="card-title">{{ $TasksDelay }}</h3>
                          </div>
                          <div class="card-footer">
                            <div class="stats">
                            <i class="material-icons">check</i> Missed tasks and due date passed
                            </div>
                          </div>
                        </div>
                      </div>
                      </div>
        <div class="row">
          <div class="col-lg-6 col-md-12">
            <div class="card">
              <div class="card-header card-header-tabs card-header-primary">
                <div class="nav-tabs-navigation">
                  <div class="nav-tabs-wrapper">
                    <span class="nav-tabs-title">My Tasks</span>

                  </div>
                </div>
              </div>
              <div class="card-body">
                <div class="tab-content">
                  <div class="tab-pane active" id="profile">
                    <table class="table">
                      <tbody>
                        <tr>
                          
                          <thead class=" text-primary">
                          <th></th>
                          <th>Title</th>
                          <th>Due Date</th>
                          </thead>
                          @foreach($tasks as $task)
                          @if($task->user_id==Auth::user()->id)

                          <td>
                          
                            <div class="form-check">
                              <label class="form-check-label">
                              <input id="{{$task->id}}" class="toggle-class form-check-input" type="checkbox" data-toggle="toggle" {{ $task->status ? 'checked' : '' }}>
                                <span class="form-check-sign">
                                  <span class="check"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          @if ($task->status == 1)
                          <td><s>{{$task->title}}</s></td>
                          @else
                          <td>{{$task->title}}</td>
                          @endif
                          <td>{{$task->due_date}}</td>
 </tr>
                          @endif
                          @endforeach
                       
                        <tr>

                      
                        </tr>
                  
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  @endsection

  @push('js')
    <script>
      $(document).ready(function() {
        // Javascript method's body can be found in assets/js/demos.js
        md.initDashboardPageCharts();
      });


      $(document).ready(function(){
           $(":checkbox").change(function(event){
                   console.log(event);
                   $.ajax({
                   url:"{{url('tasks')}}" + '/' + event.target.id,
                   dataType: 'json',
                   type: 'put' ,
                   contentType: 'application/json',
                   data: JSON.stringify({'status':event.target.checked, _token:'{{csrf_token()}}'}),
                   processData:false,
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
           });
       });
</script>
  @endpush