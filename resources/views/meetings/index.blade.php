@extends('layouts.app')
@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<h1>Homepage</h1>


<h3><a href = "{{route('create')}}">Create a new meeting </a></h3>

    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Meeting Title</th>
            <th scope="col">Location</th>
            <th scope="col">Date</th>
            <th scope="col">Time</th>
            <th scope="col">Edit</th>

        </tr>
        </thead>
        <tbody>
        @foreach($meetings as $meeting)
            <tr>
                <th scope="row">{{$meeting->id}}</th>
                <td><a href = "{{route('meetings.edit',$meeting->id)}}">{{$meeting->title}}</td>
                <td>{{$meeting->location}}</td>
                <td>{{$meeting->date}}</td>
                <td>{{$meeting->time}}</td>
                <td> <a href = "{{route('meetings.edit',$meeting->id)}}">Edit </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
