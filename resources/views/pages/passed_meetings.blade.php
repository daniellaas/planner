@extends('layouts.app', ['activePage' => 'passed_meetings', 'titlePage' => __('Meeting List')])

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">My Meetings</h4>
            <p class="card-category"> Previous Meetings</p>
            <div class="row">

                </div>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
              <div class="col-12 text-right">
                  </div>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Meeting Title</th>
            <th scope="col">Location</th>
            <th scope="col">Date & Time</th>
            <th scope="col">Length</th>

        </tr>
        <tbody>
        @foreach($meetings as $meeting)
            <tr>
                <th scope="row">{{$meeting->id}}</th>
                <td><a href="{{route('details', $meeting->id)}}">{{$meeting->title}}</a></td>
                <td>{{$meeting->location}}</td>
                <td>{{$meeting->date}}</td>
                <td>{{$meeting->length}} hours</td>

               
            </tr>
        @endforeach
        </tbody>
    </table>
           
      </div>
    </div>
  </div>
</div>
@endsection
