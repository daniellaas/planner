<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    
    public function tasks()
    {
        return $this->hasMany('App\Task');
    }
 
    public function organization()
   {
       return $this->belongsTo('App\Organization', 'org_id');
   }
    public function meetingCreator()
    {
        return $this->hasMany('App\Meeting','creator_id');
    }
    public function meetingInvited()
    {
        return $this->belongsToMany('App\Meeting','invites');
    }
    public function topics()
    {
        return $this->hasMany('App\Topic');
    }
    public function passedMeetings()
    {
        return $this->belongsToMany('App\Meeting','invites')->whereRaw('NOW()>DATE_ADD(`date`,INTERVAL `length` HOUR)');
    }
    public function futureMeetings()
    {
        return $this->belongsToMany('App\Meeting','invites')->whereRaw('NOW()<DATE_ADD(`date`,INTERVAL `length` HOUR)');
    }
    


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'org_id', 'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
