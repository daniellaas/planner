<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Organization;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'org_id'=> [ 'nullable', 'max:255'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],

        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $request)
    {
        if(!empty($request['org_id'])){
            return User::create([
                'name' => $request['name'],
                'email' => $request['email'],
                'org_id' => $request['org_id'],
                'role' => 'participant',
                'password' => Hash::make($request['password']),
            ]);
           }
           else{
               $user= User::create([
                'name' => $request['name'],
                'email' => $request['email'],
                'role' => 'manager',
                'password' => Hash::make($request['password']),
            
            ]);
    
               $organization = new Organization;
               $organization->org_name = $request['org_name'];
               $organization->min_topics = $request['min_topics'];
               $organization->address = 'home';

               $organization->creator_id = $user->id;
               $organization->save();
    
               $user->update(['org_id'=>$organization->id]);

               return $user;
           }
 
    }

}
