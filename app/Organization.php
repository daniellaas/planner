<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    protected $table='orgs';
    public function tasks()
    {
        return $this->hasMany('App\Task');
    }

    protected $fillable = [
        'org_name', 'min_topics'
    ];
}
