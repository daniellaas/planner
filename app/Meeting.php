<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meeting extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User','creator_id');
    }
    public function tasks()
    {
        return $this->hasMany('App\Task');
    }
    protected $fillable = [
    'title','location','date','length'
    ];
    protected $casts =[
    'date' => 'datetime'
    ];
   

}
