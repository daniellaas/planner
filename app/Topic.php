<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    public $timestamps = false;

    protected $guarded=[];
    public function user()
   {
       return $this->belongsTo('App\User');
   }

   public function meeting()
   {
       return $this->belongsTo('App\Meeting');
   }
   protected $fillable = [
    'status','meeting_id','title'
    ];

}
