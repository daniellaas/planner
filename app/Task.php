<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
        'status','meeting_id','title'
        ];
    public function user()
   {
       return $this->belongsTo('App\User');
   }
   public function creator()
   {
       return $this->belongsTo('App\User');
   }

   public function meeting()
   {
       return $this->belongsTo('App\Meeting');
   }


}
